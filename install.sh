#!/bin/sh

COMFYTARGET=${HOME}/bin
COMFYPATH=${PWD}/comfybin

if ! [ -d "comfybin" ]
then
    echo "please run ./comfy.sh from comfy dir"
	exit 1
fi


if ! [ -d ${COMFYTARGET} ]
then
	mkdir ${COMFYTARGET}
fi

for COMFYSCRIPT in $(ls comfybin)
do
	ln -sf ${COMFYPATH}/${COMFYSCRIPT} ${COMFYTARGET}/
done

